import { container, primaryColor } from "assets/jss/nextjs-material-kit.js";

const footerStyle = {
  leftt: {
    textAlign: "left",
    color: "#fff"
  },
  h1: {
    color: "red"
  },
  search: {
    position: 'relative',
    borderRadius: 20,

    display: "flex",
    justifyContent: "space-around",
    '&:hover': {
      // backgroundColor: "red",
    },
    marginRight: 2,
    marginLeft: 0,
    width: '100%',
    // padding: "10px"
    // [theme.breakpoints.up('sm')]: {
    //   marginLeft: theme.spacing(3),
    //   width: 'auto',
    // },
  },
  inputRoot: {
    color: 'white',
    width: '100%',
  },
  inputInput: {
    // padding: (1, 1, 1, 7),
    backgroundColor: "#86888a",
    padding: "11px",
    width: '100%',
    // transition: theme.transitions.create('width'),
    // width: '100%',
    // [theme.breakpoints.up('md')]: {
    //   width: 200,
    // },
  },
  block: {
    color: "inherit",
    padding: "0.9375rem",
    fontWeight: "500",
    fontSize: "12px",
    textTransform: "uppercase",
    borderRadius: "3px",
    textDecoration: "none",
    position: "relative",
    display: "block"
  },
  left: {
    float: "left!important",
    display: "block"
  },
  right: {
    padding: "15px 0",
    margin: "0",
    float: "right!important",
    marginLeft: "60px"
  },
  footer: {
    padding: "4rem 0",
    textAlign: "center",
    display: "flex",
    zIndex: "2",
    position: "relative"
  },
  a: {
    color: primaryColor,
    textDecoration: "none",
    backgroundColor: "transparent"
  },
  footerWhiteFont: {
    "&,&:hover,&:focus": {
      color: "#FFFFFF"
    }
  },
  container,
  list: {
    marginBottom: "0",
    padding: "0",
    marginTop: "0"
  },
  inlineBlock: {
    display: "inline-block",
    padding: "0px",
    width: "auto"
  },
  icon: {
    width: "18px",
    height: "18px",
    position: "relative",
    top: "3px"
  }
};
export default footerStyle;
