import { container } from "assets/jss/nextjs-material-kit.js";
import { fade, makeStyles } from '@material-ui/core/styles';
const componentsStyle = {

  container,
  brand: {
    color: "#FFFFFF",
    textAlign: "left"
  },
  title: {
    fontSize: "4.2rem",
    fontWeight: "600",
    display: "inline-block",
    position: "relative"
  },
  subtitle: {
    fontSize: "1.313rem",
    maxWidth: "510px",
    margin: "10px 0 0"
  },
  main: {
    background: "#FFFFFF",
    position: "relative",
    zIndex: "3"
  },
  mainRaised: {
    margin: "-60px 30px 0px",
    borderRadius: "6px",
    boxShadow:
      "0 16px 24px 2px rgba(0, 0, 0, 0.14), 0 6px 30px 5px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(0, 0, 0, 0.2)",
    "@media (max-width: 830px)": {
      marginLeft: "10px",
      marginRight: "10px"
    }
  },
  link: {
    textDecoration: "none"
  },
  textCenter: {
    textAlign: "center"
  },
  search: {
    position: 'relative',
    borderRadius: 20,

    display: "flex",
    justifyContent: "space-around",
    '&:hover': {
      // backgroundColor: "red",
    },
    marginRight: 2,
    marginLeft: 0,
    width: '100%',
    // padding: "10px"
    // [theme.breakpoints.up('sm')]: {
    //   marginLeft: theme.spacing(3),
    //   width: 'auto',
    // },
  },

  inputRoot: {
    color: 'inherit',
    width: '100%',
  },
  inputInput: {
    // padding: (1, 1, 1, 7),
    backgroundColor: "#fff3",
    padding: "11px",
    width: '100%',
    // transition: theme.transitions.create('width'),
    // width: '100%',
    // [theme.breakpoints.up('md')]: {
    //   width: 200,
    // },
  },
};

export default componentsStyle;
