import React from 'react';
// import Nav from "./Nav";
import Meta from './Meta';
import styled, { ThemeProvider, createGlobalStyle, } from 'styled-components';
import Header from './Header/Header';
import HeaderLinks from "./Header/HeaderLinks.js";
// import Header from '../mu-components/Header/Header';
// import HeaderLinks from "../mu-components/Header/HeaderLinks";
const theme = {
  red: '#FF0000',
  black: '#393939',
  grey: '#3A3A3A',
  lightgrey: '#E1E1E1',
  offWhite: '#EDEDED',
  maxWidth: '1400px',
  bs: '0 12px 24px 0 rgba(0, 0, 0, 0.09)',
};

const StyledPage = styled.div`
  background-color: #fff;
 color: ${props => props.theme.black};
`;
const GlobalStyle = createGlobalStyle`
@font-face {
    font-family: 'radnika_next';
    src: url('/radnikanext-medium-webfont.woff2') format('woff2');
    font-weight: normal;
    font-style: normal;
  }
  html {
    box-sizing: border-box;
    font-size: 10px;
  }
  *, *:before, *:after {
    box-sizing: inherit;
  }
  body {
    padding: 0;
    margin: 0;
    font-size: 1.5rem;
    line-height: 2;
    /* font-family: 'radnika_next'; */
    font-family: Arial, Helvetica, sans-serif
  }
  a {
    text-decoration: none;
    color: ${theme.black};
  }
  button {  font-family: 'radnika_next'; }
`;
const dashboardRoutes = [];
const Page = ({ children, ...rest }) => {
  return (
    <div>
      <Header
        brand="Motor Sellers"
        rightLinks={<HeaderLinks />}
        fixed
        color="white"
        changeColorOnScroll={{
          height: 400,
          color: "white"
        }}
        {...rest}
      />
      <Meta />
      {children}

    </div>

  );
};

export default Page;