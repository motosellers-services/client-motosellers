import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles({
  card: {
    maxWidth: 700,
  },
  media: {
    height: 140,
  },
});

export default function MediaCard(props) {
  const classes = useStyles();
  const { car, view } = props
  return (
    <Card className={classes.card} style={{ marginBottom: "10px" }}>
      <CardActionArea>
        <CardMedia
          className={classes.media}
          image={car.img}
          title={car.title}
          style={{ width: 250 }}
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            {car.title}
          </Typography>
          <div style={{ display: "flex", justifyContent: "space-between", alignItems: "center" }}>

            <Typography variant="body2" color="textSecondary" component="p">
              {car.model}
            </Typography>

          </div>

        </CardContent>
      </CardActionArea>
    </Card>
  );
}
