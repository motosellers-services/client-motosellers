/*eslint-disable*/
import React from "react";
// nodejs library to set properties for components
import PropTypes from "prop-types";
// nodejs library that concatenates classes
import classNames from "classnames";
// material-ui core components
import { List, ListItem } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import InputBase from '@material-ui/core/InputBase';
import { Button } from "@material-ui/core";
// @material-ui/icons
import Favorite from "@material-ui/icons/Favorite";

import styles from "assets/jss/nextjs-material-kit/components/footerStyle.js";
import styled from "styled-components";

const H1 = styled.h1`
  color: red;
  font-weight: bold;
  > span {
    color: #fff
  }
`;
const WorkingHours = styled.div`
margin: 30px 0;
  > h3 {
    margin: 0
  }
`;

const useStyles = makeStyles(styles);

export default function Footer(props) {
  const classes = useStyles();
  const { whiteFont } = props;
  const footerClasses = classNames({
    [classes.footer]: true,
    [classes.footerWhiteFont]: whiteFont
  });
  const aClasses = classNames({
    [classes.a]: true,
    [classes.footerWhiteFont]: whiteFont
  });
  return (
    <footer className={footerClasses} style={{ backgroundColor: "#313233" }}>
      <div className={classes.container}>
        <div className={classes.left}>
          <div className={classes.leftt}>
            <H1>CAR<span>STAND</span></H1>
            <WorkingHours>
              <h3>Working Hours:</h3>
              <h3>Monday - Saturday: 08 AM - 06 PM</h3>
            </WorkingHours>
            <div>
              <h3>We have some news for you, just subscribe</h3>
              <div className={classes.search}>
                <InputBase
                  placeholder="Enter Email Address"
                  classes={{
                    root: classes.inputRoot,
                    input: classes.inputInput,
                  }}
                  inputProps={{ 'aria-label': 'search' }}
                />
                <Button variant="contained" color="red" style={{ backgroundColor: "red", color: "#fff" }}>
                  Subscribe
                  </Button>
              </div>
            </div>

          </div>


        </div>
        <div className={classes.right} style={{ textAlign: "left", color: "#fff" }}>
          <h3>SERVICES</h3>
          <h3>LINKS</h3>
          <h3>CONTACTS</h3>
        </div>
      </div>
    </footer>
  );
}

Footer.propTypes = {
  whiteFont: PropTypes.bool
};
