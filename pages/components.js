import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// react components for routing our app without refresh
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// @material-ui/icons
// core components
import Footer from "components/Footer/Footer.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
// import Button from "components/CustomButtons/Button.js";
import { Button } from "@material-ui/core";
import Parallax from "components/Parallax/Parallax.js";
import InputBase from '@material-ui/core/InputBase';
// sections for this page
import ImageCard from '../components/ImageCard'
import styles from "assets/jss/nextjs-material-kit/pages/components.js";
import styled from 'styled-components';

const Inner = styled.div`
  max-width: 1300px
`;
const useStyles = makeStyles(styles);

const cars = [
  {
    img: "/static/car6.jpeg",
    title: "Toyota TLC 200",
    model: "SUV",
    price: 2000
  },
  {
    img: "/static/car7.jpeg",
    title: "Toyota TLC 200",
    model: "SUV",
    price: 2000
  },
  {
    img: "/static/car9.jpg",
    title: "Toyota TLC 200",
    model: "SUV",
    price: 2000
  },
  {
    img: "/static/car010.jpg",
    title: "Toyota TLC 200",
    model: "SUV",
    price: 2000
  },
]

const cars1 = [
  {
    img: "/static/car6.jpeg",
    title: "Toyota TLC 200",
    model: "SUV",
    price: 2000
  },
  {
    img: "/static/car7.jpeg",
    title: "Toyota TLC 200",
    model: "SUV",
    price: 2000
  },
  {
    img: "/static/car9.jpg",
    title: "Toyota TLC 200",
    model: "SUV",
    price: 2000
  },
  {
    img: "/static/car010.jpg",
    title: "Toyota TLC 200",
    model: "SUV",
    price: 2000
  },
  {
    img: "/static/car8.jpg",
    title: "Toyota TLC 200",
    model: "SUV",
    price: 2000
  },
  {
    img: "/static/car6.jpeg",
    title: "Toyota TLC 200",
    model: "SUV",
    price: 2000
  },
  {
    img: "/static/car7.jpeg",
    title: "Toyota TLC 200",
    model: "SUV",
    price: 2000
  },
  {
    img: "/static/car9.jpg",
    title: "Toyota TLC 200",
    model: "SUV",
    price: 2000
  },
]
const BackgroundText = styled.div`
  /* margin: 0 auto; */
  display: flex;
  flex-direction: column;
  align-items: center;
  line-height: 1;
  margin-bottom: 20px;
  margin-top: -80px
`;
const ButtonDiv = styled.div`
  display: flex;
  justify-content: space-around;
  width: 100%;
  margin-top: 30px;
  Button {
    padding: 10px;
  }
`;
const BestDeals = styled.div`
  padding: 50px 0;
  /* height: 500px; */
  /* background-color: pink */
`;

const CarMap = styled.div`
display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
`;

const AvailableDeals = styled.div`
padding: 10px;
   background-color: #E1E1E1 ;
`;
const HybridText = styled.div`
 background-image: linear-gradient(90deg,#E1E1E1, #3A3A3A  );
  height: 270px;
  padding: 30px 0;
`;

const Div = styled.div`
display: flex;
flex-direction: row;
justify-content: space-between;
align-items: center;
> div {
  /* width: 60%; */
  
}
> div > h1 {
  color: red;
  font-weight: bold;
  font-size: 40;
  word-spacing: 3px;
  letter-spacing: 3px
}
> div > p {
  color: #fff;
  font-weight: normal;
  font-size: 30;
  word-spacing: 3px;
  letter-spacing: 3px
}
`;
const WhiteSpace = styled.div`
  height: 100px;
  background-color: #fff
`;
export default function Components(props) {
  const classes = useStyles();
  const { ...rest } = props;
  return (
    <div>
      <Parallax image={require("assets/img/Vehicle-Dealer.jpg")}>
        <div className={classes.container}>
          <GridContainer>
            <GridItem>
              <div className={classes.brand} style={{ margin: ` 0 auto` }}>
                <BackgroundText>
                  <h3 style={{ textAlign: "center" }}>Welcome to</h3>
                  <h1 className={classes.title} style={{ marginTop: 0, marginBottom: 0 }}>Motosellers.com</h1>
                  <h3 className={classes.subtitle}>
                    Home of Naija motosellers.
                </h3>
                </BackgroundText>

                <div className={classes.search}>
                  <InputBase
                    placeholder="Search…"
                    classes={{
                      root: classes.inputRoot,
                      input: classes.inputInput,
                    }}
                    inputProps={{ 'aria-label': 'search' }}
                  />
                  <Button variant="contained" color="red" style={{ backgroundColor: "red", color: "#fff" }}>
                    Search
                  </Button>
                </div>
                <ButtonDiv>
                  <Button variant="contained" color="primary" style={{ backgroundColor: "red", color: "#fff" }}>
                    Want to be a motoseller? Click here
                  </Button>
                  <Button variant="contained" color="red" style={{ backgroundColor: "#fff", color: "red" }}>
                    Need to buy a car? click here
                  </Button>
                </ButtonDiv>
              </div>
            </GridItem>
          </GridContainer>
        </div>
      </Parallax>

      <div className={classNames(classes.main)}>
        <div className={classes.container}>

          <BestDeals>
            <div>
              <h1>Best Deals</h1>
              <p>Short Selection of our  best sellers and our most loved cars</p>
            </div>
            <CarMap>
              {cars.map(car =>
                <ImageCard car={car} />
              )}
            </CarMap>

          </BestDeals>

        </div>
        <HybridText>
          <Div className={classes.container} >
            <img src="static/car4.png" alt="car image" width={400} />
            <div>
              <h1>Motosellers Hybrid</h1>
              <p>All new 6-speed transmission & Fuel economy.</p>
            </div>
          </Div>
        </HybridText>
        <WhiteSpace>

        </WhiteSpace>
        <AvailableDeals>
          <div className={classes.container} >
            <div>
              <h1>Available Cars </h1>
              <p>Short Selection of our  best sellers and our most loved cars.</p>
            </div>
            <CarMap>
              {cars1.map(car =>
                <ImageCard car={car} view />
              )}
            </CarMap>
          </div>
        </AvailableDeals>



      </div>
      <Footer />
    </div>
  );
}
