import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// react components for routing our app without refresh
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// @material-ui/icons
// core components
import Footer from "components/Footer/Footer.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
// import Button from "components/CustomButtons/Button.js";
import { Button } from "@material-ui/core";
import Parallax from "components/Parallax/Parallax.js";
import InputBase from '@material-ui/core/InputBase';
// sections for this page
import ImageCard from '../components/ImageCard'
import styles from "assets/jss/nextjs-material-kit/pages/components.js";
import styled from 'styled-components';

const useStyles = makeStyles(styles);



const ButtonDiv = styled.div`
  display: flex;
  justify-content: space-around;
  width: 60%;
  margin-top: 30px;
  Button {
    padding: 2px;
  }
`;



const HybridText = styled.div`
background-image: linear-gradient(90deg,#F4F4F4,#F0EAEA );
  height: 270px;
  padding: 30px 0;
`;



const UploadBtn = styled.button`
background-image: linear-gradient(90deg,#F4F4F4,#F0EAEA );
  height: 40px;
  padding: 30px 0;
`;
const ProfileImage = styled.div`
  height: 10em;
  background-color: red;
  border-radius:100%;
  width: 10%;
  justify-content: "center";
  align-content:"center";
`;

const ContainerDiv = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: "space-between";
  width: "100%";

`;

const Div = styled.div`
display: flex;
flex-direction: row;
justify-content: space-between;
align-items: center;
text-align:"center";
background-color: "red";
> div {
  width: 60%;
  float: "center";
  text-align:"center";
}
`;

const PlanBtn = styled.button`
  height: 40px;
  /* border-radius: "10rem"; */
  border: "none";
  font-size: "bold";
  font-weight: "2em";
`;






const ShopInfo = styled.div`
/* display: flex;
  flex-direction: column; */
  width: 15%;
  /* height: 4em; */
`

const dealer = (props) => {
  const classes = useStyles();
  const { ...rest } = props;
  return (
   <div >
   <div style={{ marginTop: "10%", height:"100%"}}>
      <div className={classes.container}>
          <GridContainer style={{backgroundColor: "#fff", marginBottom: "3rem", height:"10em"}}>
          
            <GridItem>
              <ContainerDiv 
              style={{ 
                     justifyContent: "space-around",
                     padding: "15px 0"
                  }}
              >
              <ProfileImage style={{ 
                     alignItems: "center",
                     width: "12%",
                     height: "7em",
                     justifyContent: "center",
                     border: "4px  solid #fff",
                     boxShadow:'1px 1px 1px  1px rgba(0, 0, 0, 0.09)'
                  }}>
                  <img src = "/static/cam-white2.png" style={{ width: "70%", height: "70%", marginTop:"1em", marginLeft: "1em"}} />
                  
                </ProfileImage>
              <ShopInfo style={{ 
                   marginLeft: "-50px",
                   padding: "10px 0",
                  }}>
                <h5 style={{color:"black", fontSize:"20px", marginBottom: "-12px", fontWeight: "bolder", fontSize: "24px" }}>Shop Name</h5>
                <h5 style={{color:"red", textDecoration:"underline"}}>Shop details</h5>
              </ShopInfo>

                <ButtonDiv>
                  <PlanBtn variant="contained" color="primary" 
                  style={{ 
                    backgroundColor: "red", 
                  color: "#fff", 
                  borderRadius:"5px", 
                  border: "none",
                  fontSize: "bold",
                  fontWeight: "20px",
                  padding: "10px 10px",
                  boxShadow:'1px 1px 1px  1px rgba(0, 0, 0, 0.09)'  
                  }}>
                    Choose a plan to start uploading
                  </PlanBtn>
                  <UploadBtn variant="contained" color="red" 
                  style={{ 
                    backgroundColor: "#fff25", 
                    color: "#fff15", 
                  borderRadius:"5px", 
                  border: "none",
                  fontSize: "bold",
                  fontWeight: "20px",
                  padding: "10px 10px",
                  boxShadow:'1px 1px 1px  1px rgba(0, 0, 0, 0.09)' 
                  }}
                  >
                    Upload image/products
                  </UploadBtn>
                </ButtonDiv>
              </ContainerDiv>
            </GridItem>
          </GridContainer>
          <HybridText>
          <Div style={{alignItems: "center", justifyContent: "center" }} >
            
            <div style={{ textAlign: "center", }}>
              <p style={{}} >No uploaded files yet!</p>
              
            </div>
          </Div>
        </HybridText>
        </div>
    </div>
   </div>
  );
};

export default dealer;





